export class OrderLine {
    constructor({ uuid = ``, position, unit_uuid = ``,  tax_uuid = ``, unit_price_net = 0, order_uuid = ``, linked_entity_uuid = ``, linked_entity = ``,  type = ``, label = ``, qty = ``, payload = ``} = {}) {
        this.uuid = uuid;
        this.position = (position < 0) ? 0 : (position > 255) ? 255 : position;
        this.unit_uuid = unit_uuid;
        this.tax_uuid = tax_uuid;
        this.unit_price_net = unit_price_net;
        this.order_uuid = order_uuid;
        this.linked_entity_uuid = linked_entity_uuid;
        this.type = type;
        this.label = label;
        this.linked_entity = linked_entity;
        this.qty = qty;
        this.payload = payload;
    }
}

export function createOrderLine(data) {
    return Object.freeze(new OrderLine(data));
}