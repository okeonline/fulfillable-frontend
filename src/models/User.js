export class User {
    constructor({ uuid = ``, email = ``, name = ``,  username = ``, password = `` } = {}) {
        this.uuid = uuid;
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
    }
}

export function createUser(data) {
    return Object.freeze(new User(data));
}