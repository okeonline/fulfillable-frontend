export class Supplier {
    constructor({ uuid = ``, company_name = ``, contact_name = ``, email = `` } = {}) {
        this.uuid = uuid;
        this.company_name = company_name;
        this.contact_name = contact_name;
        this.email = email;
    }
}

export function createSupplier(data) {
    return Object.freeze(new Supplier(data));
}