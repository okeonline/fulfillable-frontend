import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/auth'
import toast from './modules/toast'
import orders from './modules/orders'
import orderLines from './modules/order-lines'
import orderableItems from './modules/orderableItems'
import orderTransactions from './modules/order-transactions'
import orderDocuments from './modules/order-documents'
import customers from './modules/customers'
import customerGroups from './modules/customer-groups'
import customerAddresses from './modules/customer-addresses'
import paymentMethods from './modules/payment-methods'
import services from './modules/services'
import shippingMethods from './modules/shipping-methods'
import shippingTypes from './modules/shipping-types'
import suppliers from './modules/suppliers'
import products from './modules/products'
import productBrands from './modules/product-brands'
import productCategories from './modules/product-categories'
import taxes from './modules/taxes'
import users from './modules/users'
import flows from './modules/flows'
import swimlanes from './modules/swimlanes'

import {axios, api} from '@/api/laravel'
import router from "@/router";

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

//https://markus.oberlehner.net/blog/how-to-structure-a-complex-vuex-store/
const vuexStore = new Vuex.Store({
    modules: {
        auth,
        toast,
        orders,
        orderLines,
        orderableItems,
        orderTransactions,
        orderDocuments,
        customers,
        customerGroups,
        customerAddresses,
        paymentMethods,
        services,
        shippingMethods,
        shippingTypes,
        suppliers,
        products,
        productBrands,
        productCategories,
        taxes,
        users,
        flows,
        swimlanes
    },
    strict: debug,
    // plugins: debug ? [createLogger()] : []f
})

vuexStore.$api = api;

// Response interceptors for API calls
axios.interceptors.request.use(function (config) {
    const token = vuexStore.getters['auth/getBearerToken']
    if (token) config.headers.Authorization = 'Bearer ' + token;
    return config;
});

axios.interceptors.response.use((response) => {
    return response
}, async function (error) {

    switch (error.response.status) {
        case 401: //401: Token ongeldig > opnieuw inloggen
            vuexStore.dispatch('toast/warning', 'Uw sessie is verlopen, u dient opnieuw in te loggen.');
            await vuexStore.dispatch('auth/unset');
            await router.push({
                name: 'login',
            });
            break;

        case 403: //403: Niet genoeg rechten > toon 'geen rechten' pagina
            await router.push({
                name: 'error-NotPermitted',
            });
            break;

        case 406: //406: Niet aanvaardbaar a.k.a. de inloggegevens zijn onjuist
            vuexStore.dispatch('toast/warning', 'Inloggegevens onjuist');
            await vuexStore.dispatch('auth/unset');
            await router.push({
                name: 'login',
            });
            break;

        case 418: //418: Token verlopen
            //try {
            //    if (error.config.headers['No-Retry'] !== true) {
            //        await vuexStore.dispatch('auth/refresh');
            //        return await axios({...error.config, headers: {...error.config.headers, 'No-Retry': true}});
            //    }
            //} catch {
            //FYI: Automatic token-refresh mechanism is out of order since api keeps rethrowing 418.
                vuexStore.dispatch('toast/warning', 'Uw sessie kon niet worden verlengd, u dient opnieuw in te loggen.');
                await vuexStore.dispatch('auth/unset');
                await router.push({
                    name: 'login',
                });
            //}

            break;

        case 422: //422: Validatiefout > terugkoppeling naar formulier "één van de velden klopt niet"
            vuexStore.dispatch('toast/error', 'Aanvraag mislukt, één of meerdere velden zijn onjuist ingevuld');
            break;

        case 503: //503: Maintenance mode
            vuexStore.dispatch('toast/error', 'De applicatie staat momenteel in onderhoudsmodus, probeer het later opnieuw.');
            await vuexStore.dispatch('auth/unset');
            await router.push({
                name: 'error-maintenance',
            });
            break;

        case 400: //400: Operatie mislukt :sad_doctor:
        default:
            await router.push({
                name: 'error-NotPermitted',
            });
            vuexStore.dispatch('toast/error', {
                text: 'De aanvraag is mislukt',
                data: {
                    'api request': JSON.parse(error.response.config.data),
                    'api response': error.response.data,
                }
            });
    }

    return Promise.reject(error);
});

vuexStore.dispatch('auth/checkToken');

export default vuexStore;