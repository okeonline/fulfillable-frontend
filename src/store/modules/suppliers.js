import {createSupplier} from '@/models/Supplier';
import {basestate, basegetters, baseactions, basemutations} from '@/store/basemodule'
import cloneDeep from 'lodash/cloneDeep'

const state = () => ({
    ...cloneDeep(basestate),
    entityName: 'suppliers',
    singleEntityName: 'supplier',
    pluralEntityName: 'suppliers',

    schema: [
        {
            entity: 'uuid',
            label: 'UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false
        },
        {
            entity: 'company_name',
            label: 'Bedrijfsnaam',
            type: 'String',
            edit: true,
            list: true,
            validation: [
                v => !!v || 'Bedrijfsnaam is vereist'
            ]
        },
        {
            entity: 'contact_name',
            label: 'Contactpersoon',
            type: 'String',
            edit: true,
            list: true,
            validation: [
                v => !!v || 'Contactpersoon is vereist'
            ]
        },
        {
            entity: 'email',
            label: 'Email',
            type: 'String',
            edit: true,
            list: true,
            validation: [
                v => !!v || 'Email is vereist'
            ]
        }
    ]
})

export const getters = {
    ...basegetters
}

export const actions = {
    ...baseactions,
    async storeEntity(vuex, payload) {
        await actions.storeEntityBase(vuex, createSupplier(payload), this.$api);
    },
}

export const mutations = {
    ...basemutations,
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}