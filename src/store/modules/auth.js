import {createCredentials} from "@/models/Auth";

const parseJWT = (jwt) => jwt ? JSON.parse(atob(jwt.split('.')[1])) : '';
const localBearerToken = localStorage.getItem('bearerToken'); //only for initialization of state!

//BearerToken will be inside localstorage
const state = () => ({
    bearerToken: localBearerToken || '',
    name: parseJWT(localBearerToken).name,
    role: parseJWT(localBearerToken).role || 'Gebruiker',
    loading: false,
    error: false,
    success: false,
    entityName: 'auth',
    singleEntityName: 'auth',
    pluralEntityName: 'auths',
})

const getters = {
    getBearerToken: state => {
        return state.bearerToken
    },
    getUserData: state => {
        return { name: state.name, role: state.role }
    }
}

const actions = {
    checkToken({state}) {
        if(state.bearerToken) {
            //This is a facade, the real check is done inside middleware.
            this.$api.getEntity({entityName: 'auth', uuid: 'me'}, ()=>{}, ()=>{});
        }
    },
    async login({commit}, payload) {
        commit('setLoading', true);
        let credentials = createCredentials(payload);
        await this.$api.authenticate({action: 'login', payload: credentials},
            (response) => {
                commit('setBearerToken', response.access_token);
                commit('setUserData');
                commit('setLoading', false);
            },
            errors => {
                console.log(errors);
            })
    },
    async refresh({state, commit}) {
        commit('setLoading', true);

        await this.$api.authenticate({action: 'refresh', payload: state.bearerToken},
            (response) => {
                commit('setBearerToken', response.access_token);
                commit('setUserData');
                commit('setLoading', false);
            },
            errors => {
                console.log(errors);
            })
    },
    async logout({state, commit}) {
        commit('setLoading', true);
        await this.$api.authenticate({action: 'logout', payload: state.bearerToken},
        () => {
            commit('removeBearerToken')
            commit('setLoading', false);
        },
        errors => {
            console.log(errors);
        })
    },
    set({commit}, bearerToken) {
        commit('setBearerToken', bearerToken);
        commit('setUserData');
    },
    unset({commit}) {
        commit('removeBearerToken');
    }
}

const mutations = {
    setUserData(state) {
        state.name = parseJWT(state.bearerToken).name;
        state.role = parseJWT(state.bearerToken).role || 'Gebruiker';
    },
    setBearerToken(state, bearerToken) {
        state.bearerToken = bearerToken;
        localStorage.setItem('bearerToken', bearerToken);
    },
    removeBearerToken(state) {
        localStorage.removeItem('bearerToken');
        state.bearerToken = '';
    },
    setLoading(newstate) {
        state.loading = newstate;
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}