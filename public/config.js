// eslint-disable-next-line no-unused-vars
const config = (() => {
    return {
        "DEBUG":false,
        "APP_NAME":"FulFillable",
        "APP_VARIANT":"custom_webkarpet",// default / custom_webkarpet
        "APP_ORG":"Webkarpet",
        "API_URL":"http://api.koala.okedev.nl/api/", //https://webkarpet.fulfillable.nl/api/
        "THEME": {
            "DARK": false,              // Overrules DARK_NAVIGATION, experimental!
            "DARK_NAVIGATION": true
        },
        "LOGGING": {
            "ENABLED": true,
            "ENVIRONMENT":"development",// production / staging / development
            "TRACING": true,            // Performance monitoring
            "DSN":"https://d766c9c64a234b2b8888a99e878d899c@o501909.ingest.sentry.io/5614003",
        }
    };
})();