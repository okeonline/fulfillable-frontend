import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);
/* Primary color cannot be set inside oo_variables.. */
export default new Vuetify({
    customVariables: ['~/scss/oo_variables.scss'],
    theme: {
        "dark": config.THEME.DARK,
        "themes": {
            "light": {
                "primary": "#23223A",
                "secondary": "#e62a49"
            },
            "dark": {
                "primary": "#23223A",
                "secondary": "#e62a49"
            }
        }
    }
});
