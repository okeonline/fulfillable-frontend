export class ProductBrand {
    constructor({ uuid = ``, name = `` } = {}) {
        this.uuid = uuid;
        this.name = name;
    }
}

export function createProductBrand(data) {
    return Object.freeze(new ProductBrand(data));
}