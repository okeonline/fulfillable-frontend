import { shallowMount } from '@vue/test-utils'
import ooJsonTemplateEngine from '@/components/common/ooJsonTemplateEngine/index.vue'
import ooTinyTest from '@/components/common/ooTinyTest/index.vue'
import {mapActions} from "vuex";

const onButtonClick = jest.fn();

const FunctionalWrapper = {
    name: 'template_engine_test',
    mixins: [ooJsonTemplateEngine],
    components: {
        ooTinyTest,
    },
    methods: {
        onButtonClick
    },
    computed: {
        contentViaComputed() {
            return [{
                _uid: "d0d4bc75-208e-487d-9294-6d9e1bbb719e",
                component: "oo-tiny-test",
                events: {
                    "click.native": "onButtonClick",
                },
            }];
        }
    }
}

describe('ooJsonTemplateEngine', () => {
    it('Renders components', () => {
        const wrapper = shallowMount(FunctionalWrapper);

        let buttonExists = wrapper.findComponent(ooTinyTest).exists();

        expect(buttonExists).toBe(true);
    })

    it('Renders components with events', () => {
        const wrapper = shallowMount(FunctionalWrapper);

        wrapper.findComponent(ooTinyTest).trigger('click');

        expect(onButtonClick).toHaveBeenCalledTimes(0); //ToDo: does not work?
    })
})
