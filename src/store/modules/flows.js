import {createFlow} from '@/models/Flow';
import {basestate, basegetters, baseactions, basemutations} from '@/store/basemodule'
import cloneDeep from 'lodash/cloneDeep'

const state = () => ({
    ...cloneDeep(basestate),
    entityName: 'flows',
    singleEntityName: 'flow',
    pluralEntityName: 'flows',

    schema: [
        {
            entity: 'uuid',
            label: 'UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false
        },
        {
            entity: 'inbox_swimlane_uuid',
            label: 'INBOX UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false
        },
        {
            entity: 'completed_swimlane_uuid',
            label: 'COMPLETED UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false
        },
        {
            entity: 'name',
            label: 'Naam',
            type: 'String',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Naam is vereist'
            ]
        },
        {
            entity: 'color',
            label: 'Kleur',
            type: 'String',
            edit: false,
            hide: false,
            list: false,
            validation: []
        },
        {
            entity: 'swimlanes_count',
            label: 'Hoeveelheid swimlanes',
            type: 'Number',
            edit: false,
            hide: true,
            list: true,
            validation: []
        }
    ]
})

export const getters = {
    ...basegetters,
    swimlanes: (state,getters,rootState,rootGetters) => (flow_uuid) => {
        return rootGetters['swimlanes/getLocalEntitiesByProps']({flow_uuid: flow_uuid.toString()}) ?? [];
    }
}

export const actions = {
    ...baseactions,
    async fetchOrders(vuex, payload) {
        await vuex.dispatch('orders/fetchForRelation', {source:vuex.state.entityName, value:payload}, {root:true});
    },
    async fetchSwimlanes(vuex, payload) {
        await vuex.dispatch('swimlanes/fetchForRelation', {source:vuex.state.entityName, value:payload}, {root:true});
        //await vuex.dispatch('swimlanes/fetch', {filter:{flow_uuid: payload}}, {root:true});
    },
    async storeEntity(vuex, payload) {
        let second_payload = {...payload};
        //payload._swimlanes = payload._swimlanes.filter((e) => e._action !== undefined);
        let flow_result = await actions.storeEntityBase(vuex, createFlow(payload), this.$api);

        payload._swimlanes.map(function (e){
            e.flow_uuid = flow_result.affectedUuid;
            second_payload.uuid = flow_result.affectedUuid;
        });

        if(payload._swimlanes !== undefined){
            let creatables = payload._swimlanes.filter((e) => e._action === 'create');
            let updatables = payload._swimlanes.filter((e) => e._action === 'update');
            let deletables = payload._swimlanes.filter((e) => e._action === 'delete');

            await Promise.all(
                await creatables.map(async function (e) {
                    let swimlane = {...e};
                    delete swimlane.uuid;
                    let result = await vuex.dispatch('swimlanes/storeEntity', swimlane, {root:true});
                    if(swimlane.type === 'inbox') second_payload.inbox_swimlane_uuid = result.affectedUuid;
                    if(swimlane.type === 'completed') second_payload.completed_swimlane_uuid  = result.affectedUuid;
                })
            );

            await Promise.all(
                await updatables.map(async function (swimlane) {
                    await vuex.dispatch('swimlanes/storeEntity', swimlane, {root:true});
                })
            );

            await Promise.all(
                await deletables.map(async function (swimlane) {
                    await vuex.dispatch('swimlanes/deleteEntity', swimlane, {root:true});
                })
            );
        }

        await actions.storeEntityBase(vuex, createFlow(second_payload), this.$api);
    },
}

export const mutations = {
    ...basemutations,
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}