//import {createSwimlane} from "@/models/Swimlane";

export class Flow {
    constructor({ uuid = ``,inbox_swimlane_uuid = ``,completed_swimlane_uuid = ``, name = ``, color = `` } = {}) {
        this.uuid = uuid;
        this.inbox_swimlane_uuid = inbox_swimlane_uuid;
        this.completed_swimlane_uuid = completed_swimlane_uuid;
        this.name = name;
        this.color = color;
        /*this._swimlanes = _swimlanes;*/
    }
}

export function createFlow(data) {
    /*const _swimlanes = (data._swimlanes !== undefined) ? data._swimlanes.map(x => createSwimlane(x)) : [];*/
    return Object.freeze(new Flow({...data})); //,_swimlanes
}