import {createCustomerAddress} from '@/models/CustomerAddress';
import {basestate, basegetters, baseactions, basemutations} from '@/store/basemodule'
import cloneDeep from 'lodash/cloneDeep'

const state = () => ({
    ...cloneDeep(basestate),
    entityName: 'customer-addresses',
    singleEntityName: 'customer-address',
    pluralEntityName: 'customer-addresses',

    schema: [
        {
            entity: 'uuid',
            label: 'UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false,
            validation: []
        },
        {
            entity: 'type',
            label: 'Type',
            type: 'Select',
            edit: true,
            hide: false,
            list: true,

            //quick fix
            multipleOptions: true,
            options:[
                'shipping', 'billing'
            ],

            validation: [
                v => !!v || 'Type is vereist'
            ]
        },
        {
            entity: 'customer_uuid',
            label: 'Klant',
            type: 'String',
            edit: true,
            hide: true,
            list: false,
            validation: [
                v => !!v || 'Klant is vereist'
            ]
        },
        {
            entity: 'name',
            label: 'Naam',
            type: 'String',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Naam is vereist'
            ]
        },
        {
            entity: 'street_line_1',
            label: 'Straatnaam',
            type: 'String',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Adresregel 1 is vereist'
            ]
        },
        {
            entity: 'street_line_2',
            label: 'Adresregel 2',
            type: 'String',
            edit: true,
            hide: true,
            list: false,
            validation: []
        },
        {
            entity: 'street_number',
            label: 'Huisnummer',
            type: 'String',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Huisnummer is vereist',
                v => !isNaN(v) || 'Huisnummer moet numeriek zijn'
            ]
        },
        {
            entity: 'street_number_addition',
            label: 'Toevoeging huisnummer',
            type: 'String',
            edit: true,
            hide: false,
            list: true,
            validation: []
        },
        {
            entity: 'zipcode',
            label: 'Postcode',
            type: 'String',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Postcode is vereist'
            ]
        },
        {
            entity: 'city',
            label: 'Stad',
            type: 'String',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Stad is vereist'
            ]
        },
        {
            entity: 'region',
            label: 'Regio',
            type: 'String',
            edit: false,
            hide: false,
            list: false,
            validation: []
        },
        {
            entity: 'country',
            label: 'Land',
            type: 'Select',
            edit: true,
            hide: false,
            list: true,

            //quick fix
            options:[
                'Nederland',
                'België',
                'Bulgarije',
                'Cyprus',
                'Denemark­en',
                'Duitsland',
                'Estland',
                'Finland',
                'Frankrijk',
                'Griekenland',
                'Hongarije',
                'Ierland',
                'Italië',
                'Kroatië',
                'Letland',
                'Litouwen',
                'Luxemburg',
                'Malta',
                'Oostenrijk',
                'Polen',
                'Portugal',
                'Roemenië',
                'Slovenië',
                'Slowakije',
                'Spanje',
                'Tsjechië',
                'Zweden',
                'Albanië',
                'Andorra',
                'Armenië',
                'Azerbeid­zjan',
                'Bosnië en Herzegovina',
                'Georgië',
                'IJsland',
                'Liechten­stein',
                'Moldavië',
                'Monaco',
                'Montenegro',
                'Noord-Macedonië',
                'Noorwegen',
                'Oekraïne',
                'Rusland',
                'San Marino',
                'Servië',
                'Turkije',
                'Verenigd Koninkrijk',
                'Zwitserland',
                'Overigen',
                'Kazachstan',
                'Kosovo',
                'Vaticaanstad',
                'Wit-Rusland'
            ],

            validation: [
                v => !!v || 'Land is vereist'
            ]
        }
    ]
})

export const getters = {
    ...basegetters
}

export const actions = {
    ...baseactions,
    async storeEntity(vuex, payload) {
        if(Array.isArray(payload.type)) {
            await Promise.all(
                payload.type.map(async e => {
                    await actions.storeEntityBase(vuex, createCustomerAddress({...payload, type:e}), this.$api);
                })
            );
        }
        else {
            await actions.storeEntityBase(vuex, createCustomerAddress(payload), this.$api);
        }
    },
}

export const mutations = {
    ...basemutations,
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}