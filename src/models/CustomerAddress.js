export class CustomerAddress {
    constructor({ uuid = ``, type = ``, customer_uuid = ``, name = ``, street_line_1 = ``, street_line_2 = ``, street_number = ``, street_number_addition = ``, zipcode = ``, city = ``, region = ``, country = `` } = {}) {
        this.uuid = uuid;
        this.type = type === 'billing' ? 'billing' : 'shipping';
        this.customer_uuid = customer_uuid;
        this.name = name;
        this.street_line_1 = street_line_1;
        this.street_line_2 = street_line_2;
        this.street_number = street_number;
        this.street_number_addition = street_number_addition;
        this.zipcode = zipcode;
        this.city = city;
        this.region = region;
        this.country = country;
    }
}

export function createCustomerAddress(data) {
    return Object.freeze(new CustomerAddress(data));
}