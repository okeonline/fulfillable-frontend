import {createShippingType} from '@/models/ShippingType';
import {basestate, basegetters, baseactions, basemutations} from '@/store/basemodule'
import cloneDeep from 'lodash/cloneDeep'

const state = () => ({
    ...cloneDeep(basestate),
    entityName: 'shipping-types',
    singleEntityName: 'shipping-type',
    pluralEntityName: 'shipping-types',

    schema: [
        {
            entity: 'uuid',
            label: 'UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false
        },
        {
            entity: 'name',
            label: 'Naam',
            type: 'String',
            edit: true,
            list: true,
            validation: [
                v => !!v || 'Naam is vereist'
            ]
        }
    ]
})

export const getters = {
    ...basegetters
}

export const actions = {
    ...baseactions,
    async storeEntity(vuex, payload) {
        await actions.storeEntityBase(vuex, createShippingType(payload), this.$api);
    },
}

export const mutations = {
    ...basemutations,
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}