import {createOrderLine} from '../../models/OrderLine';
import {basestate, basegetters, baseactions, basemutations} from '@/store/basemodule'
import cloneDeep from 'lodash/cloneDeep'

const state = () => ({
    ...cloneDeep(basestate),
    entityName: 'order-lines',
    singleEntityName: 'order-line',
    pluralEntityName: 'order-lines',

    schema: [
        {
            entity: 'uuid',
            label: 'UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false
        },
        {
            entity: 'order_uuid',
            label: 'Order UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false
        },
        {
            entity: 'position',
            label: 'Volgorde',
            type: 'Number',
            edit: false,
            hide: false,
            list: false,
            searchable: false
        },
        {
            entity: 'type',
            label: 'Type',
            type: 'String',
            edit: true,
            hide: true,
            list: false,
            validation: [
                v => !!v || 'Type is vereist'
            ]
        },
        {
            entity: 'qty',
            label: 'Aantal',
            type: 'Number',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Aantal is vereist',
                v => !isNaN(v) || 'Aantal  moet numeriek zijn'
            ]
        },
        {
            entity: 'label',
            label: 'Naam',
            type: 'String',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Naam is vereist',
            ]
        },
        {
            entity: 'unit_uuid',
            label: 'Eenheid',
            type: 'String',
            edit: true,
            hide: true,
            list: false,
            validation: []
        },
        {
            entity: 'linked_entity',
            label: 'Gelinkte entiteit',
            type: 'String',
            edit: true,
            hide: true,
            list: false,
            validation: [
                v => !!v || 'Gelinkte entiteit is vereist'
            ]
        },
        {
            entity: 'linked_entity_uuid',
            label: 'FK Gelinkte entiteit',
            type: 'String',
            edit: true,
            hide: true,
            list: false,
            validation: [
                v => !!v || 'FK Gelinkte entiteit is vereist'
            ]
        },
        {
            entity: 'payload',
            label: 'Inhoud',
            type: 'String',
            edit: false,
            hide: false,
            list: false,
            validation: []
        },
        {
            entity: 'unit_price_net',
            label: 'Nettoprijs per eenheid',
            type: 'Currency',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => (v === 0 || !!v) || 'Nettoprijs is vereist'
                /*v => !isNaN(v) || 'Nettoprijs moet numeriek zijn'*/
            ]
        },
        {
            entity: 'tax_uuid',
            label: 'BTW tarief',
            type: 'Tax',
            edit: true,
            hide: false,
            list: false,
            validation: [
                v => !!v || 'BTW Tarief is vereist',
            ]
        },
        {
            entity: 'created_at',
            label: 'Aangemaakt op',
            type: 'Date',
            edit: false,
            hide: false,
            list: false,
            validation: [
            ]
        }
    ]
})

const getters = {
    ...basegetters
}

const actions = {
    ...baseactions,
    async storeEntity(vuex, payload) {
        await actions.storeEntityBase(vuex, createOrderLine(payload), this.$api);
    },
}

const mutations = {
    ...basemutations,
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}