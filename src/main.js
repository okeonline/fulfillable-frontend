import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from './router'
import store from './store'
import moment from 'moment'

import * as Sentry from "@sentry/browser";
import { Integrations } from "@sentry/tracing";

Sentry.init({
  Vue: Vue,
  dsn: config.LOGGING.DSN,
  autoSessionTracking: true,
  enabled: config.LOGGING.ENABLED,
  environment: config.LOGGING.ENVIRONMENT,
  integrations: [
    new Integrations.BrowserTracing(),
  ],
  tracingOptions: {
    trackComponents: config.LOGGING.TRACING,
  },
  logErrors: true,
  tracesSampleRate: 1.0,
});

// eslint-disable-next-line no-unused-vars
Vue.config.errorHandler = function (err, vm, info) {
  Sentry.captureException(err);
}

Vue.prototype.$moment = moment
Vue.config.productionTip = false

export const eventbus = new Vue();

new Vue({
  vuetify,
  router,
  store,
  render: h => h(App)
}).$mount('#app');

