//const axios = require('axios').default;
import axios from 'axios'

let createFilterQuery = (filter, firstChar = "?") => {
    let str = firstChar;
    for (let key in filter) {
        if (str.length > 1) str += "&";
        str += "filter[" + key + "]=" + encodeURIComponent(filter[key]);
    }
    if (str.length > 1) return str;
    return "";
}

const api = {
    getEntities({entityName, filter}, cb, errorCb) {
        let filterQuery = createFilterQuery(filter, "");
        return axios.get(config.API_URL + entityName + '?page[size]=9999&page[number]=1' + filterQuery)
            .then(function (response) {
                // handle succes
                cb(response.data);
                return response.data;
            })
            .catch(function (error) {
                errorCb(error);
            })
    },

    getPaginatedEntities({entityName, page, filter}, cb, errorCb) {
        let filterQuery = createFilterQuery(filter, "&");
        return axios.get(config.API_URL + entityName + '?page[size]=50&page[number]=' + page + filterQuery)
            .then(function (response) {
                // handle succes
                cb(response.data);
                return response.data;
            })
            .catch(function (error) {
                errorCb(error);
            })
    },

    getEntityRelation({entity, relation, relationUUID, filter = {}}, cb, errorCb) {
        let filterQuery = createFilterQuery(filter);
        return axios.get(config.API_URL + relation + '/' + relationUUID + '/_' + entity + filterQuery )
            .then(function (response) {
                // handle succes
                cb(response.data);
                return response.data;
            })
            .catch(function (error) {
                errorCb(error);
            })
    },

    getEntity({entityName, uuid, filter = {}}, cb, errorCb) {
        let filterQuery = createFilterQuery(filter);
        return axios.get(config.API_URL + entityName + '/' + uuid + filterQuery)
            .then(function (response) {
                cb(response.data.data);
                return response.data;
            })
            .catch(function (error) {
                errorCb(error);
            })
    },

    postEntity({entityName, payload}, cb, errorCb) {
        return axios.post(config.API_URL + entityName, payload)
            .then(function (response) {
                cb(response.data);
                return response.data;
            })
            .catch(function (error) {
                errorCb(error);
            })
    },

    patchAction({entityName, payload}, cb, errorCb) {
        let {uuid, action, identifiers, content} = {uuid: '', action: '', identifiers: [], content:'', ...payload};
        let actionUrl = (action) ? '/'  + action : '';
        let uuidUrl = (uuid) ?  '/'  + uuid : '';
        let identifierUrl = (identifiers.length > 0) ? '/' + identifiers.join('/') : '';

        //PATCH api/orders/{:uuid}/placeInFlow/{:flow_uuid}
        return axios.patch(config.API_URL + entityName + uuidUrl + actionUrl + identifierUrl, content)
            .then(function (response) {
                cb(response.data);
                return response.data;
            })
            .catch(function (error) {
                errorCb(error);
            })
    },

    putEntity({entityName, payload}, cb, errorCb) {
        return axios.put(config.API_URL + entityName + '/' + payload.uuid, payload)
            .then(function (response) {
                cb(response.data);
                return response.data;
            })
            .catch(function (error) {
                errorCb(error);
            })
    },

    deleteEntity({entityName, payload}, cb, errorCb) {
        return axios.delete(config.API_URL + entityName + '/' + payload.uuid)
            .then(function (response) {
                cb(response.data);
                return response.data;
            })
            .catch(function (error) {
                errorCb(error);
            })
    },

    authenticate({action, payload}, cb, errorCb) {
        return axios.post(config.API_URL + 'auth/' + action, payload, {headers: {'No-Retry': true}})
            .then(function (response) {
                cb(response.data);
                return response.data;
            })
            .catch(function (error) {
                errorCb(error);
            })
    }
}

export {axios, api}