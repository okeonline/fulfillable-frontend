# vuetifytest

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Installation
``` 
npm install -g @vue/cli
```
``` 
vue create . 
```
``` 
vue add vuetify 
```
### Note
Vue3 (beta) is incompatible with vuetify, therefore we're currently using Vue2.

### References

https://vueschool.io/articles/vuejs-tutorials/structuring-vue-components/

https://dev.to/lampewebdev/vuejs-pages-with-dynamic-layouts-problems-and-a-solution-4460/

https://dev.to/pikax/dynamic-components-using-vuejs-4b78/
