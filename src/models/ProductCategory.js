export class ProductCategory {
    constructor({ uuid = ``, name = `` } = {}) {
        this.uuid = uuid;
        this.name = name;
    }
}

export function createProductCategory(data) {
    return Object.freeze(new ProductCategory(data));
}