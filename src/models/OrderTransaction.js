import moment from 'moment'
export class OrderTransaction {
    constructor({ uuid = ``, payment_method_uuid = ``, payment_number = ``,  amount = ``, state = `open`,  order_uuid = ``,  completed_at = `` } = {}) {
        this.uuid = uuid;
        this.order_uuid = order_uuid; //refactor: was transaction_id
        this.payment_method_uuid = payment_method_uuid;
        this.payment_number = payment_number;
        this.amount = amount;
        this.state = state;
        this.completed_at = moment(completed_at).format('YYYY-M-DD');
    }
}

export function createOrderTransaction(data) {
    return Object.freeze(new OrderTransaction(data));
}