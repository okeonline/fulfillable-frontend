export class Auth {
    constructor({ email = ``, password = `` } = {}) {
        this.email = email;
        this.password = password;
    }
}

export function createCredentials(data) {
    return Object.freeze(new Auth(data));
}