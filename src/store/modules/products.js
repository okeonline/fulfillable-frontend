import {createProduct} from '@/models/Product';
import {basestate, basegetters, baseactions, basemutations} from '@/store/basemodule'
import cloneDeep from 'lodash/cloneDeep'

const state = () => ({
    ...cloneDeep(basestate),
    entityName: 'products',
    singleEntityName: 'product',
    pluralEntityName: 'products',

    schema: [
        {
            entity: 'uuid',
            label: 'UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false
        },
        {
            entity: 'name',
            label: 'Naam',
            type: 'String',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Naam is vereist'
            ]
        }
    ]
})

export const getters = {
    ...basegetters
}

export const actions = {
    ...baseactions,
    async storeEntity(vuex, payload) {
        await actions.storeEntityBase(vuex, createProduct(payload), this.$api);
    },
}

export const mutations = {
    ...basemutations,
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}