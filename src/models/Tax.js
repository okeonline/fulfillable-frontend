export class Tax {
    constructor({ uuid = ``, name = ``, country = `` , rate = ``  } = {}) {
        this.uuid = uuid;
        this.name = name;
        this.country = country;
        this.rate = rate;
    }
}

export function createTax(data) {
    return Object.freeze(new Tax(data));
}