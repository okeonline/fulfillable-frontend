export class Service {
    constructor({ uuid = ``, type = ``, name = ``, characteristic = ``, unit_stock_id = ``, unit_sell_id = ``, unit_purchase_id = ``, sku = ``, code_prefix = ``, art_code = ``, stock_level_warning = ``, webshop_id = ``, product_brand_id = `` } = {}) {
        this.uuid = uuid;
        this.type = type;
        this.name = name;
        this.characteristic = characteristic;
        this.unit_stock_id = unit_stock_id;
        this.unit_sell_id = unit_sell_id;
        this.unit_purchase_id = unit_purchase_id;
        this.sku = sku;
        this.code_prefix = code_prefix;
        this.art_code = art_code;
        this.stock_level_warning = stock_level_warning;
        this.webshop_id = webshop_id;
        this.product_brand_id = product_brand_id;
    }
}

export function createService(data) {
    return Object.freeze(new Service(data));
}