export class Swimlane {
    constructor({ uuid = ``, flow_uuid, name = ``, type= `default`, color = ``, sort_order = ``, entry_action = ``, leave_action = ``, _action} = {}) {
        this.uuid = uuid;
        this.flow_uuid = flow_uuid;
        this.name = name;
        this.type = type;
        this.color = color;
        this.sort_order = sort_order;
        this.entry_action = entry_action;
        this.leave_action = leave_action;
        this._action = _action;
    }
}

export function createSwimlane(data) {
    return Object.freeze(new Swimlane({...data}));
}