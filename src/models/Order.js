import {createOrderLine} from './OrderLine';

export class Order {
    constructor({ uuid = ``, establishment = `Genemuiden`, shipping_address_uuid, billing_address_uuid, tax_uuid, customer_uuid, currency_uuid/* = `1eb39b6e-56fb-6cc0-8709-c932b25dd8d8`*/, state = ``, order_lines = [], order_telephone, order_email, note_private = ``, note_public = ``, shipping_method_uuid, shipping_type_uuid,  order_delivery_date = ``, payment_method_uuid, is_concept = false} = {}) {
        this.uuid = uuid;
        this.shipping_address_uuid = shipping_address_uuid;
        this.billing_address_uuid = billing_address_uuid;
        this.tax_uuid = tax_uuid;
        this.customer_uuid = customer_uuid;
        this.currency_uuid = currency_uuid;
        this.state = state === 'new' ? state : state === 'processing' ? state : 'new';
        this.order_lines = order_lines;
        this.order_email = order_email;
        this.order_telephone = order_telephone;
        this.note_private = note_private;
        this.note_public = note_public;

        //refactor: comes from OrderShipping
        this.shipping_method_uuid = shipping_method_uuid;
        this.shipping_type_uuid = shipping_type_uuid;
        this.order_delivery_date = order_delivery_date;

        //refactor: comes from OrderPayment
        this.payment_method_uuid = payment_method_uuid;

        this.establishment = establishment;
        this.is_concept = is_concept;
    }
}

export function createOrder(data) {
    const order_lines = (data.order_lines !== undefined) ? data.order_lines.map(x => createOrderLine(x)) : [];
    return Object.freeze(new Order({...data, order_lines}));
}