import {createCustomerGroup} from '@/models/CustomerGroup';
import {basestate, basegetters, baseactions, basemutations} from '@/store/basemodule'
import cloneDeep from 'lodash/cloneDeep'

const state = () => ({
    ...cloneDeep(basestate),
    entityName: 'customer-groups',
    singleEntityName: 'customer-group',
    pluralEntityName: 'customer-groups',

    schema: [
        {
            entity: 'uuid',
            label: 'UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false
        },
        {
            entity: 'name',
            label: 'Naam',
            type: 'String',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Naam is vereist'
            ]
        }
    ]
})

export const getters = {
    ...basegetters
}

export const actions = {
    ...baseactions,
    async storeEntity(vuex, payload) {
        await actions.storeEntityBase(vuex, createCustomerGroup(payload), this.$api);
    },
}

export const mutations = {
    ...basemutations,
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}