export class Unit {
    constructor({ uuid = ``,  type = ``, name = ``, short = `` , factor = ``, factor_of_unit_id = ``   } = {}) {
        this.uuid = uuid;
        this.type = type;
        this.name = name;
        this.short = short;
        this.factor = factor;
        this.factor_of_unit_id = factor_of_unit_id;
    }
}

export function createUnit(data) {
    return Object.freeze(new Unit(data));
}