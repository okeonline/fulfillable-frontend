import { shallowMount } from '@vue/test-utils'
import HelloWorld from '@/components/common/ooHelloWorld/index.vue'

describe('ooHelloWorld', () => {
  it('renders props.appName when passed', () => {
    const appName = 'ApplicationName as a string'
    const wrapper = shallowMount(HelloWorld, {
      propsData: { appName }
    })
    expect(wrapper.text()).toMatch(appName)
  })
})
