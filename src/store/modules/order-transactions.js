import {createOrderTransaction} from '../../models/OrderTransaction';
import {basestate, basegetters, baseactions, basemutations} from '@/store/basemodule'
import cloneDeep from 'lodash/cloneDeep'

const state = () => ({
    ...cloneDeep(basestate),
    entityName: 'order-transactions',
    singleEntityName: 'order-transaction',
    pluralEntityName: 'order-transactions',

    schema: [
        {
            entity: 'uuid',
            label: 'UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false
        },
        {
            entity: 'state',
            label: 'Status',
            type: 'String',
            edit: false,
            hide: false,
            list: true,
            validation: []
        },
        {
            entity: 'payment_number',
            label: 'Betalingsreferentie',
            type: 'String',
            edit: true,
            hide: false,
            list: true,
            validation: []
        },
        {
            entity: 'amount',
            label: 'Hoeveelheid',
            type: 'Number',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Hoeveelheid is vereist',
                v => !isNaN(v) || 'Hoeveelheid  moet numeriek zijn'
            ]
        },
        {
            entity: 'order_uuid',
            label: 'Order UUID',
            type: 'Number',
            edit: false,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Ordernummer is vereist'
            ]
        },
        {
            entity: 'completed_at',
            label: 'Afhandelingsdatum',
            type: 'Date',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Afhandelingsdatum is vereist'
            ]
        },
        {
            entity: 'completed',
            label: 'Voldaan',
            type: 'Boolean',
            edit: true,
            hide: false,
            list: true,
            validation: []
        }
       /* {
            entity: 'credit',
            label: 'Krediet',
            type: 'Boolean',
            edit: false,
            hide: false,
            list: true
        }*/
    ]
})

const getters = {
    ...basegetters
}

const actions = {
    ...baseactions,
    async storeEntity(vuex, payload) {
        await actions.storeEntityBase(vuex, createOrderTransaction(payload), this.$api);
    },
}

const mutations = {
    ...basemutations,
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}