const webpack = require('webpack');
//const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],
  configureWebpack: {
    plugins: [
      new webpack.IgnorePlugin({
        resourceRegExp: /^\.\/locale$/,
        contextRegExp: /moment$/
      })
      /*new BundleAnalyzerPlugin()*/
    ],
    resolve: {
      alias: {
        moment$: 'moment/moment.js'
      }
    }
  },
  pluginOptions: {
    autoRouting: {
      pages: 'src/views',
      importPrefix: '@/views/',
      chunkNamePrefix: 'oo-',
      nested: false
    }
  },
  css: {
    loaderOptions: {
      scss: {
        prependData: `
          @import "~@/assets/fonts/jtp6gae.css";
          @import "~@/scss/oo_variables.scss";
          @import "~@/scss/oo_mixins.scss";
        `
      }
    },
    extract: false
  },
  productionSourceMap: process.env.NODE_ENV !== 'production'
}
