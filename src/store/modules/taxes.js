import {createTax} from '@/models/Tax';
import {basestate, basegetters, baseactions, basemutations} from '@/store/basemodule'
import cloneDeep from 'lodash/cloneDeep'

const state = () => ({
    ...cloneDeep(basestate),
    entityName: 'taxes',
    singleEntityName: 'tax',
    pluralEntityName: 'taxes',

    schema: [
        {
            entity: 'uuid',
            label: 'UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false
        },
        {
            entity: 'name',
            label: 'Naam',
            type: 'String',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Naam is vereist'
            ]
        },
        {
            entity: 'country',
            label: 'Land',
            type: 'String',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Land is vereist'
            ]
        },
        {
            entity: 'rate',
            label: 'Percentage',
            type: 'Double',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Percentage is vereist',
                v => !isNaN(v) || 'Percentage moet numeriek zijn'
            ]
        }
    ]
})

export const getters = {
    ...basegetters
}

export const actions = {
    ...baseactions,
    async storeEntity(vuex, payload) {
        await actions.storeEntityBase(vuex, createTax(payload), this.$api);
    },
}

export const mutations = {
    ...basemutations,
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}