import {basestate, basegetters, baseactions, basemutations} from '@/store/basemodule'
import cloneDeep from 'lodash/cloneDeep'

const state = () => ({
    ...cloneDeep(basestate),
    entityName: 'order-documents',
    singleEntityName: 'order-document',
    pluralEntityName: 'order-documents',

    schema: [
        {
            entity: 'uuid',
            label: 'UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false
        },
        {
            entity: 'type',
            label: 'Type',
            type: 'String',
            edit: false,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Type is vereist'
            ]
        },
        {
            entity: 'order_uuid',
            label: 'Order UUID',
            type: 'String',
            edit: false,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Ordernummer is vereist'
            ]
        },
        {
            entity: 'file_name',
            label: 'Bestandsnaam',
            type: 'String',
            edit: false,
            hide: false,
            list: true,
            validation: []
        },
        {
            entity: 'file_path',
            label: 'Bestandslocatie',
            type: 'String',
            edit: false,
            hide: false,
            list: true,
            validation: []
        },
        {
            entity: 'deeplink_code',
            label: 'Deeplink',
            type: 'String',
            edit: false,
            hide: false,
            list: true,
            validation: []
        }
    ]
})

const getters = {
    ...basegetters
}

const actions = {
    ...baseactions
}

const mutations = {
    ...basemutations,
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}