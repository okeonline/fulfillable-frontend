import {getField, updateField} from 'vuex-map-fields';
import cloneDeep from 'lodash/cloneDeep'

export const basestate = {
    //Static information
    schema: [],
    relations: [],

    //Variable information
    data: [],
    relationData: [],

    //State
    pagination: {
        total: 0,
        per_page: 0,
        current_page: 0,
        selection: []
    },
    searchQuery: '',
    loading: false,
    success: false,
    apiErrors: [],
    error: ''
}

const schemaMapper = (state) => {
    return Object.assign(...state.schema.map((e) => {
        switch (e.type) {
            case 'Date':
                return {[e.entity]: ''}

            case 'String':
            case 'Password':
                return {[e.entity]: ''}

            case 'Number':
            case 'Double':
                return {[e.entity]: 0}

            case 'Array':
                return {[e.entity]: []}

            case 'Boolean':
                return {[e.entity]: false}

            default:
                return {[e.entity]: undefined}

        }
    }))
};

const relationMapper = (state) => {
    if(state.relations.length < 1) return;
    return Object.assign(...state.relations.map((e) => {
        switch (e.type) {
            case 'one-to-many':
            case 'many-to-one':
                return {[e.module]:{}}

            default:
            case 'many-to-many':
                return {[e.module]:[]}
        }
    }))
};

// getters
export const basegetters = {
    init: (state) => {
      return schemaMapper(state);
    },
    getField,
    getLocalEntitiesByProps: (state) => (filter) => {
        let x = state.data.filter(function(item) {
            for (let key in filter) {
                if (item[key] === undefined || item[key] !== filter[key])
                    return false;
            }
            return true;
        });
        return cloneDeep(x);
    },
    getEditFields: (state) => {
        let fields = cloneDeep(state.schema.filter(entity => entity.edit))
        fields.forEach(function (field) {
            field.apiErrors = state?.apiErrors.find(entity => entity.entity === field.entity)?.errors ?? []
        });
        return fields;
    },
    getListFields: (state) => {
        return state.schema.filter(entity => entity.list)
    },
    getSearchFields: (state) => {
        let searchable =  state.schema.filter(entity => entity.searchable);
        let searchQuery = state.searchQuery;

        /*currently returning 1 record, this function is awaiting api implementation for multiple fields*/
        return searchable && searchable.length > 0 && searchQuery !== ''
            ? {[searchable[0].entity]:searchQuery}
            : {};
    }

}

// actions
export const baseactions = {
    resetFlags({commit}) {
        commit('resetFlags');
    },

    //Will be called by ui when error has been handled.
    resetApiErrors({commit}) {
        commit('updateField', {path: 'apiErrors', value: []});
    },

    setSearchQuery({commit}, payload) {
        commit('updateField', {path: 'searchQuery', value: payload});
    },

    //actions based on API
    async action({state, commit}, payload) {
        return await this.$api.patchAction({entityName: state.entityName, payload:payload},
            () => {
                //commit('upsertData', data);
                //commit('updateField', {path: 'loading', value: false});
            },
            (e) => {
                commit('updateField', {path: 'error', value: e});
            })
    },
    async fetch({state, commit}, {filter = {}}) {
        await commit('resetFlags');

        await this.$api.getEntities({entityName: state.entityName, filter},
            data => {
                commit('upsertData', data);
                commit('setPaginationMeta', data);
                commit('updateField', {path: 'loading', value: false});
            },
            (e) => {
                commit('updateField', {path: 'error', value: e});
            })
    },
    async fetchForRelation({state, commit, dispatch}, {single = false, source, value, filter = {}}) {
        await commit('resetFlags');

        await this.$api.getEntityRelation({entity: single ? state.singleEntityName : state.pluralEntityName, relation:source, relationUUID:value, filter},
            data => {
                commit('upsertData', data);
                commit('updateField', {path: 'loading', value: false});

                let updatedUuids = data.data.map(e => e.uuid);
                dispatch(source + '/storeRelationData', {uuid:value, relation:state.entityName, data:updatedUuids}, {root:true});
            },
            (e) => {
                commit('updateField', {path: 'error', value: e});
            })
    },
    async fetchEntity({state, commit}, uuid) {
        await commit('resetFlags');

        await this.$api.getEntity({entityName: state.entityName, uuid},
            data => {
                commit('upsertEntity', data[0])
                commit('updateField', {path: 'loading', value: false});
            },
            (e) => {
                commit('updateField', {path: 'error', value: e});
            });
    },
    async storeEntityRelations(vuex, payload) {
        vuex.state.relations.forEach(e => {
            if(payload[e.module_single] !== undefined) vuex.dispatch(e.module + '/storeEntity', payload[e.module_single], {root:true});
        });
    },
    async storeEntityBase({state, commit}, entity, api) {
        let onSuccess = function (response) {
            commit('updateField', {path: 'success', value: response.operation});
            commit('upsertEntity', response.affectedModel)
            commit('updateField', {path: 'loading', value: false});
        };

        let onError = function (e) {
            commit('setApiErrors', e.response);
            commit('updateField', {path: 'error', value: e});
        };

        await commit('resetFlags');

        if (entity.uuid)
            return await api.putEntity({entityName: state.entityName, payload:entity}, onSuccess, onError);
        else
            return await api.postEntity({entityName: state.entityName, payload:entity}, onSuccess, onError);

    },
    async deleteEntity({state, commit}, entity) {
        await commit('resetFlags');

        await this.$api.deleteEntity({entityName: state.entityName, payload:entity},
            (response) => {
                commit('updateField', {path: 'success', value: response.operation});
                commit('unsetEntity', response.affectedModel)
                commit('updateField', {path: 'loading', value: false});
            },
            (e) => {
                commit('setApiErrors', e.response);
                commit('updateField', {path: 'error', value: e});
            })
    },

    //actions based on internal data
    // eslint-disable-next-line no-unused-vars
    async storeRelationData({commit}, {uuid, relation, data}) {

        if(!uuid || !relation || !data)
            return;

        commit('setRelationData', {uuid, [relation]:data})
    },

    //Based on local state
    async fetchPage({state, commit}, page) {
        await commit('resetFlags');

        await this.$api.getPaginatedEntities({entityName: state.entityName, page, filter:basegetters.getSearchFields(state)},
            data => {
                commit('upsertData', data);
                commit('setPaginationMeta', data);
                commit('updateField', {path: 'loading', value: false});
            },
            (e) => {
                commit('updateField', {path: 'error', value: e});
            })
    },

    updateItem({commit}, payload) {
        commit('upsertEntity', payload)
    },
}

// mutations
export const basemutations = {
    updateField,

    upsertData(state, {data}) {
        let basicSchema = schemaMapper(state);

        //Merge with basic schema
        data.forEach(function(element, index, array) {
            array[index] = {...basicSchema, ...element};
        });

        //Upsert data in dataset
        data.forEach(function(element) {
            const i = state.data.findIndex(item => {
                return element?.uuid ? item.uuid === element.uuid : false;
            });
            if (i > -1) Object.assign(state.data[i], element);
            else state.data.push(element);
        });
    },
    upsertEntity(state, payload) {
        payload = {...schemaMapper(state), ...payload};

        const i = state.data.findIndex(item => {
            return payload?.uuid ? item.uuid === payload.uuid : false;
        });
        if (i > -1) Object.assign(state.data[i], payload);
        else state.data.push(payload);
    },
    unsetEntity(state, payload) {
        const i = state.data.findIndex(item => {
            return item.uuid === payload.uuid
        });
        if (i > -1) state.data.splice(i, 1);
    },

    setRelationData(state, payload) {
        let relationDefaults = relationMapper(state);
        let updatedRelations = [];

        Object.keys(payload).forEach((e) => {
            if(e !== 'uuid') updatedRelations.push(e)
        });

        payload = {...relationDefaults, ...payload};

        const i = state.relationData.findIndex(item => {
            return payload?.uuid ? item.uuid === payload.uuid : false;
        });

        if (i > -1) {
            updatedRelations.forEach(function (e) {
                state.relationData[i][e] = relationDefaults ? relationDefaults[e] : '';
            });
            Object.assign(state.relationData[i], payload);
        }
        else state.relationData.push(payload);
    },

    setPaginationMeta(state, {data, total, per_page, current_page}) {
        let selection = [];
        data.forEach(function(element, index) {
            selection[index] = element.uuid;
        });

        state.pagination.total = total;
        state.pagination.per_page = per_page;
        state.pagination.current_page = current_page;
        state.pagination.selection = selection;
    },
    resetFlags(state) {
        state.loading = true;
        state.success = false;
        state.apiErrors = [];
        state.error = '';
    },
    setApiErrors(state, {data}) {
        state.apiErrors = [];
        Object.keys(data.errors).forEach(function (key) {
            state.apiErrors.push({'entity': key, 'errors': data.errors[key]});
        });
    },
}