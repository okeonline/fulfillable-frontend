import {basestate, basegetters, baseactions, basemutations} from '@/store/basemodule'
import cloneDeep from 'lodash/cloneDeep'

/*ORDERABLEITEMS IS 'RETRIEVE-ONLY', DOES NOT HAVE APIMAPPER.*/
const state = () => ({
    ...cloneDeep(basestate),
    entityName: 'orderableItems',
    singleEntityName: 'orderableItem',
    pluralEntityName: 'orderableItems',

    schema: [
        {
            entity: 'uuid',
            label: 'UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false
        },
        {
            entity: 'type',
            label: 'Type',
            type: 'String',
            edit: true,
            hide: true,
            list: false,
            validation: [
                v => !!v || 'Type is vereist'
            ]
        },
        {
            entity: 'qty',
            label: 'Aantal',
            type: 'Number',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Aantal is vereist',
                v => !isNaN(v) || 'Aantal  moet numeriek zijn'
            ]
        },
        {
            entity: 'label',
            label: 'Naam',
            type: 'String',
            edit: true,
            hide: false,
            list: true,
            validation: []
        },
        {
            entity: 'unit_stock_uuid',
            label: 'Opslageenheid',
            type: 'String',
            edit: true,
            hide: true,
            list: false,
            validation: []
        },
        {
            entity: 'unit_sell_uuid',
            label: 'Verkoopeenheid',
            type: 'String',
            edit: true,
            hide: true,
            list: false,
            validation: []
        },
        {
            entity: 'unit_purchase_uuid',
            label: 'Koopeenheid',
            type: 'String',
            edit: true,
            hide: true,
            list: false,
            validation: []
        },
        {
            entity: 'unit_price_net',
            label: 'Nettoprijs per eenheid',
            type: 'Currency',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Nettoprijs is vereist'
               /* v => !isNaN(v) || 'Nettoprijs moet numeriek zijn'*/
            ]
        }
    ]
})

const getters = {
    ...basegetters
}

const actions = {
    ...baseactions
}

const mutations = {
    ...basemutations,
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}