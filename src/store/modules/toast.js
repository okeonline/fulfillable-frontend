import * as Sentry from "@sentry/browser";

const state = () => ({
    notifications: [],
})

const getters = {}

const actions = {
    success({commit}, payload) {
     commit('append', {type: 'success', text: payload});
    },
    info({commit}, payload) {
        commit('append', {type: 'info', text: payload});
    },
    warning({commit}, payload) {
        commit('append', {type: 'warning', text: payload});
    },
    dismiss({commit}, id) {
        commit('remove', id);
    },
    error({commit}, payload) {
        const regularNotification = typeof payload === "string";

        let text = regularNotification
            ? payload
            : payload?.text ?? '';

        let additionalData = !regularNotification
            ?  payload?.data
            : '';

        commit('append', {type: 'error', text: text});

        if(additionalData !== '')
            Sentry.captureException(new Error(text), {
                contexts: additionalData
            });
    },
}

const mutations = {
    append(state, notification) {
        let dt = new Date().getTime();
        let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            let r = (dt + Math.random() * 16) % 16 | 0;
            dt = Math.floor(dt / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });

        state.notifications.push({uuid: uuid, ...notification});
    },
    remove(state, uuid) {
        state.notifications = state.notifications.filter(function (e) {
            return e.uuid !== uuid
        });
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}