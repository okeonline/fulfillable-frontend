import routes from 'vue-auto-routing'
import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store/index'

Vue.use(VueRouter)

//ToDo: All this stuff has to be done before compiletime (inside vue-auto-routing) instead of runtime,
//This way stuff like the imports can be handled correctly by webpack.

function clone(object) {
    let clone = {...object};
    clone.component = {...object.component};
    clone.meta = {...object.meta};
    return clone;
}

function find_header_component(currentRoute, routes, component) {
    let matchingRoute = routes.find((route) => {
        let splitRoute = route.path.split('/');
        let lastItem = splitRoute[splitRoute.length - 1];

        if(lastItem === component) {
            splitRoute.pop();
            return currentRoute.path === splitRoute.join('/')
        }
        return false;
    });

    if(matchingRoute) return matchingRoute.component;
    return Vue.component();
}


//Bind header components to route
routes
    .forEach((route) => {
        route.meta = route.meta ? route.meta : [];
        route.children = route.children ? route.children : [];
        route.props = {default: true};
        route.components = {
            "default": route.component,
            "custom-1": find_header_component(route, routes, 'header-1'),
            "custom-2": find_header_component(route, routes, 'header-2')
        }
    });


//Converting flat routes to routes with nested (child)routes
routes
    .filter((route) => {
        let r = route.path.split('/');
        return r.length === 3
    })
    .forEach((nonNestedRoute) => {
        //determine variables
        let path = nonNestedRoute.path.split('/');
        let parentPath = '/' + path[1];
        let nestedRoute = clone(nonNestedRoute);

        //update path
        path.shift();
        path.shift();
        nestedRoute.path = path.join('/');

        //add as child to parent route
        routes.find(function (route) {
            return parentPath === route.path;
        }).children.push(nestedRoute);

        //remove from routes
        nonNestedRoute.meta.remove = true;
    });

//Removing routes
for (let i = routes.length - 1; i >= 0; --i) {
    if (routes[i].meta.remove)
        routes.splice(i, 1);
}

//order menu items by 'order'
routes.sort((a, b) => (a.meta.order > b.meta.order) ? 1 : -1);

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

//Redirect to login when necessary
router.beforeEach(async(to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters['auth/getBearerToken'] !== '') {
            //await store.dispatch('auth/refresh'); //just keep refreshin'
            next()
            return
        }
        next('/login')
    } else {
        next()
    }
})

export default router