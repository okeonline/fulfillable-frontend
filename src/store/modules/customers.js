import {createCustomer} from '../../models/Customer';
import {basestate, basegetters, baseactions, basemutations} from '@/store/basemodule'
import cloneDeep from 'lodash/cloneDeep'

const state = () => ({
    ...cloneDeep(basestate),
    entityName: 'customers',
    singleEntityName: 'customer',
    pluralEntityName: 'customers',

    schema: [
        {
            entity: 'uuid',
            label: 'UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false,
            searchable: false
        },
        {
            entity: 'customer_number',
            label: 'Klantnummer',
            type: 'Number',
            edit: false,
            hide: true,
            list: true,
            searchable: false
        },
        {
            entity: 'checked',
            label: 'checked',
            type: 'Boolean',
            edit: false,
            hide: true,
            list: false,
            searchable: false
        },
        {
            entity: 'customer_group_uuid',
            label: 'Klantgroep',
            type: 'String',
            edit: false,
            hide: false,
            list: false,
            searchable: false,
            validation: [
                v => !!v || 'Klantgroep is vereist'
            ]
        },
        {
            entity: 'name',
            label: 'Naam',
            type: 'String',
            edit: true,
            hide: false,
            list: true,
            searchable: true,
            validation: [
                v => !!v || 'Naam is vereist'
            ]
        },
        {
            entity: 'company_name',
            label: 'Bedrijfsnaam',
            type: 'String',
            edit: true,
            hide: false,
            list: false,
            searchable: false,
            validation: []
        },
        {
            entity: 'email',
            label: 'E-Mail adres',
            type: 'String',
            edit: true,
            hide: false,
            list: false,
            searchable: false,
            validation: [
                v => !!v || 'E-mail is vereist',
                v => /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(v) || 'E-Mail moet een geldig e-mailadres zijn',
                v => v.length < 256 || 'E-Mail mag niet langer zijn dan 255 karakters'
            ]
        },
        {
            entity: 'email_billing',
            label: 'E-Mail adres facturatie',
            type: 'String',
            edit: true,
            hide: false,
            list: false,
            searchable: false,
            validation: [
                v => !!v || 'Facturatie E-mail is vereist',
                v => /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(v) || 'Facturatie E-mail moet een geldig e-mailadres zijn',
                v => v.length < 256 || 'Facturatie E-mail mag niet langer zijn dan 255 karakters'

            ]
        },
        {
            entity: 'phone_number',
            label: 'Telefoonnummer',
            type: 'String',
            edit: true,
            hide: false,
            list: false,
            searchable: false,
            validation: [
                v => !!v || 'Telefoonnummer is vereist'
            ]
        },
        {
            entity: 'note',
            label: 'Notitie',
            type: 'String',
            edit: true,
            hide: false,
            list: false,
            validation: []
        },

        //Computed
        {
            entity: 'defaultAddressZipcode',
            label: 'Postcode',
            type: 'String',
            edit: false,
            hide: true,
            list: true,
        },
        {
            entity: 'defaultAddressStreetNumber',
            label: 'Huisnummer',
            type: 'String',
            edit: false,
            hide: true,
            list: true,
            searchable: false
        },
        {
            entity: 'customerGroupName',
            label: 'Klantgroep',
            type: 'String',
            edit: false,
            hide: true,
            list: true,
            searchable: false
        }
    ],
    relations: [
        {
            module: 'customerAddresses',
            module_multiple: 'customer-addresses',
            type: 'one-to-many'
        }
    ]
})

export const getters = {
    ...basegetters,
    getAddressesByUuid: (state, getters, rootState, rootGetters) => (payload) => {
        let uuid = getters.getLocalEntitiesByProps({uuid:payload})[0]?.uuid;
        if(uuid === undefined) return [];
        return rootGetters['customerAddresses/getLocalEntitiesByProps']({customer_uuid: uuid.toString()}) ?? [];
    },
    getOrdersByUuid: (state, getters, rootState, rootGetters) => (payload) => {
        let uuid = getters.getLocalEntitiesByProps({uuid:payload})[0]?.uuid;
        if(uuid === undefined) return [];
        return rootGetters['orders/getLocalEntitiesByProps']({customer_uuid: uuid.toString()}) ?? [];
    }
}

export const actions = {
    ...baseactions,
    async fetchAddresses(vuex, payload) {
        //let uuid = vuex.getters.getLocalEntityByUuid(payload).uuid;
        await vuex.dispatch('customerAddresses/fetchForRelation', {source:vuex.state.entityName, value:payload}, {root:true});
    },
    async fetchOrders(vuex, payload) {
        await vuex.dispatch('orders/fetchForRelation', {source:vuex.state.entityName, value:payload}, {root:true});
    },
    async storeEntity(vuex, payload) {
        let customer = await actions.storeEntityBase(vuex, createCustomer(payload), this.$api);
        //await actions.storeEntityRelations(vuex, payload);
        return customer;
    },
}

export const mutations = {
    ...basemutations,
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}