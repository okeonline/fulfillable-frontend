export class CustomerGroup {
    constructor({ uuid = ``, name = `` } = {}) {
        this.uuid = uuid;
        this.name = name;
    }
}

export function createCustomerGroup(data) {
    return Object.freeze(new CustomerGroup(data));
}