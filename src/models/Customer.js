import {createCustomerAddress} from "@/models/CustomerAddress";

export class Customer {
    constructor({ uuid = ``, company_name = ``, name = `` , email = `` , email_billing = ``, customer_group_uuid = ``, phone_number = ``, customer_addresses = ``, note = ``} = {}) {
        this.uuid = uuid;
        this.company_name = company_name;
        this.name = name;
        this.email = email;
        this.email_billing = email_billing;
        this.customer_group_uuid = customer_group_uuid;
        this.phone_number = phone_number;
        this.customer_addresses = customer_addresses;
        this.note = note;
    }
}

export function createCustomer(data) {
    const customer_addresses = data.customer_addresses !== undefined
        ? data.customer_addresses.map(x => createCustomerAddress(x))
        : [];
    return Object.freeze(new Customer({...data, customer_addresses}));
}