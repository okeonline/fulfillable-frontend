import {createOrder} from '../../models/Order';
import {basestate, basegetters, baseactions, basemutations} from '@/store/basemodule'
import cloneDeep from 'lodash/cloneDeep'

const state = () => ({
    ...cloneDeep(basestate),
    entityName: 'orders',
    singleEntityName: 'order',
    pluralEntityName: 'orders',

    schema: [
        {
            entity: 'uuid',
            label: 'UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false,
            searchable: false
        },
        {
            entity: 'checked',
            label: 'checked',
            type: 'Boolean',
            edit: false,
            hide: true,
            list: false,
            searchable: false
        },
        {
            entity: 'order_number',
            label: 'Ordernummer',
            type: 'String',
            edit: false,
            hide: true,
            list: true,
            searchable: true
        },
        {
            entity: 'customer_uuid',
            label: 'Klantnummer',
            type: 'Number',
            edit: true,
            hide: false,
            list: false,
            searchable: false
        },
        {
            entity: 'shipping_method_uuid',
            label: 'Methode',
            type: 'Number',
            edit: true,
            hide: false,
            list: false,
            searchable: false
        },
        {
            entity: 'shipping_type_uuid',
            label: 'Soort',
            type: 'Number',
            edit: true,
            hide: false,
            list: false,
            searchable: false
        },
        {
            entity: 'order_created_date',
            label: 'Datum',
            type: 'Date',
            edit: false,
            hide: false,
            list: false,
            searchable: false
        },
        {
            entity: 'order_delivery_date',
            label: 'Ontvangstdatum',
            type: 'Date',
            edit: true,
            hide: false,
            list: false,
            searchable: false
        },
        {
            entity: 'payment_method_uuid',
            label: 'Betaalmethode',
            type: 'String',
            edit: true,
            hide: false,
            list: false,
            searchable: false
        },
        {
            entity: 'order_telephone',
            label: 'Telefoonnummer',
            type: 'String',
            edit: true,
            hide: false,
            list: false,
            searchable: false
        },
        {
            entity: 'customer_uuid',
            label: 'Klantnummer',
            type: 'Number',
            edit: true,
            hide: false,
            list: false,
            searchable: false
        },
        {
            entity: 'order_lines',
            label: 'Orderregels',
            type: 'Array',
            edit: true,
            hide: false,
            list: false,
            searchable: false
        },
        {
            entity: 'customer_name',
            label: 'Klantnaam',
            type: 'String',
            edit: false,
            hide: true,
            list: false,
            searchable: false
        },
        {
            entity: 'note_private',
            label: 'Opmerkingen intern',
            type: 'String',
            edit: true,
            hide: false,
            list: false,
            searchable: false
        },
        {
            entity: 'note_public',
            label: 'Opmerkingen extern',
            type: 'String',
            edit: true,
            hide: false,
            list: false,
            searchable: false
        },
        {
            entity: 'state',
            label: 'Orderstatus',
            type: 'String',
            edit: false,
            hide: true,
            list: true,
            searchable: false
        },

        //Computed
        {
            entity: 'customerName',
            label: 'Naam',
            type: 'String',
            edit: false,
            hide: true,
            list: true,
            searchable: false
        },
        {
            entity: 'totals-totalGross',
            label: 'Orderbedrag',
            type: 'Number',
            edit: false,
            hide: true,
            list: true,
            searchable: false
        },
        {
            entity: 'warning',
            label: 'Waarschuwing',
            type: 'Boolean',
            edit: false,
            hide: true,
            list: false,
            searchable: false
        },
        {
            entity: 'order_shipping_date',
            label: 'Verzenddatum',
            type: 'Date',
            edit: false,
            hide: true,
            list: false,
            searchable: false
        },
        {
            entity: 'establishment',
            label: 'Vestiging',
            type: 'Select',
            edit: true,
            hide: false,
            list: config.APP_VARIANT !== 'custom_webkarpet',

            multipleOptions: false,
            options:[
                'Genemuiden', 'Oudenbosch'
            ],

            validation: [
                v => !!v || 'Vestiging is vereist'
            ]
        },
    ],

    relations: [
        {
            module: 'orderTransactions',
            module_multiple: 'order-transactions',
            type: 'one-to-many',
        }
    ]
})

const getters = {
    ...basegetters,
    orderLines: (state,getters,rootState,rootGetters) => (order_uuid) => {
        return rootGetters['orderLines/getLocalEntitiesByProps']({order_uuid: order_uuid.toString()}) ?? [];
    },
    // eslint-disable-next-line no-unused-vars
    orderTransactions: (state,getters,rootState,rootGetters) => (order_uuid) => {
        //return {...rootGetters['orderTransactions/getLocalEntitiesByProps']({order_uuid: order_uuid}), order_uuid};
        return rootGetters['orderTransactions/getLocalEntitiesByProps']({order_uuid: order_uuid.toString()}) ?? [];
    }
}

const actions = {
    ...baseactions,
    async fetchCustomerAddress(vuex, payload) {
        await vuex.dispatch('customerAddresses/fetchForRelation', {source:'customers', value:payload}, {root:true});
    },
    async fetchProducts(vuex, payload) {
        await vuex.dispatch('orderLines/fetchForRelation', {source:vuex.state.entityName, value:payload, filter:{linked_entity: 'product'}}, {root:true});
    },
    async fetchGenerics(vuex, payload) {
        await vuex.dispatch('orderLines/fetchForRelation', {source:vuex.state.entityName, value:payload, filter:{linked_entity: 'generic'}}, {root:true});
    },
    async fetchServices(vuex, payload) {
        await vuex.dispatch('orderLines/fetchForRelation', {source:vuex.state.entityName, value:payload, filter:{linked_entity: 'service'}}, {root:true});
    },
    /*async fetchUpholstry(vuex, payload) {
        await vuex.dispatch('upholstry/fetchForRelation', {source:vuex.state.entityName, value:payload}, {root:true});
    },*/
    async fetchOrderTransactions(vuex, payload) {
        await vuex.dispatch('orderTransactions/fetchForRelation', {source:vuex.state.entityName, value:payload}, {root:true});
    },
    async fetchOrderDocuments(vuex, payload) {
        await vuex.dispatch('orderDocuments/fetchForRelation', {source:vuex.state.entityName, value:payload}, {root:true});
    },
    async fetchCustomer(vuex, payload) {
        await vuex.dispatch('customers/fetchForRelation', {source:vuex.state.entityName, value:payload}, {root:true});
    },
    // eslint-disable-next-line no-unused-vars
    async storeEntity(vuex, payload) {
        let order = await actions.storeEntityBase(vuex, createOrder(payload), this.$api);


        if(payload.order_transactions !== undefined){
            let creatables = payload.order_transactions.filter((e) => e.storeAction === 'create');
            let removables = payload.order_transactions.filter((e) => e.storeAction === 'remove');

            await Promise.all(
                await creatables.map(async function (e) {
                    let transaction = {...e};
                    delete transaction.uuid;
                    await vuex.dispatch('orderTransactions/storeEntity', transaction, {root:true});
                })
            );

            await Promise.all(
                await removables.map(async function (transaction) {
                    await vuex.dispatch('orderTransactions/deleteEntity', transaction, {root:true});
                })
            );
        }

        if(payload.order_flow !== undefined && order.affectedModel.inFlows[0] !== payload.order_flow)
            await vuex.dispatch('action', {uuid: order.affectedUuid, action: 'placeOrderInFlow', identifiers:[payload.order_flow]});

        await actions.storeEntityRelations(vuex, payload);
        return order;
    },
}

const mutations = {
    ...basemutations,
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}