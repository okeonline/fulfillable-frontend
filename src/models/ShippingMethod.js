export class ShippingMethod {
    constructor({ uuid = ``, name = `` } = {}) {
        this.uuid = uuid;
        this.name = name;
    }
}

export function createShippingMethod(data) {
    return Object.freeze(new ShippingMethod(data));
}