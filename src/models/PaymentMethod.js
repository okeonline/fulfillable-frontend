export class PaymentMethod {
    constructor({ uuid = ``, name = `` } = {}) {
        this.uuid = uuid;
        this.name = name;
    }
}

export function createPaymentMethod(data) {
    return Object.freeze(new PaymentMethod(data));
}