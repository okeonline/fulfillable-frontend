import {createUser} from '../../models/User';
import {basestate, basegetters, baseactions, basemutations} from '@/store/basemodule'
import cloneDeep from 'lodash/cloneDeep'

const state = () => ({
    ...cloneDeep(basestate),
    entityName: 'users',
    singleEntityName: 'user',
    pluralEntityName: 'users',

    schema: [
        {
            entity: 'uuid',
            label: 'UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false,
        },
        {
            entity: 'name',
            label: 'Naam',
            type: 'String',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Naam is vereist'
            ]
        },
        {
            entity: 'username',
            label: 'Gebruikersnaam',
            type: 'String',
            edit: false,
            hide: false,
            list: false,
            validation: [
                v => !!v || 'Gebruikersnaam is vereist'
            ]
        },
        {
            entity: 'email',
            label: 'Email',
            type: 'String',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'E-mail is vereist',
                v => /.+@.+/.test(v) || 'E-mail is ongeldig',
            ]
        },
        {
            entity: 'password',
            label: 'Wachtwoord',
            type: 'Password',
            edit: true,
            hide: false,
            list: false,
            validation: [
                v => !!v || 'Wachtwoord is vereist'
            ]
        }
    ]
})

const getters = {
    ...basegetters
}

const actions = {
    ...baseactions,
    async storeEntity(vuex, payload) {
        await actions.storeEntityBase(vuex, createUser(payload), this.$api);
    },
}

const mutations = {
    ...basemutations,
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}