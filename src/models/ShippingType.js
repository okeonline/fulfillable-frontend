export class ShippingType {
    constructor({ uuid = ``, name = `` } = {}) {
        this.uuid = uuid;
        this.name = name;
    }
}

export function createShippingType(data) {
    return Object.freeze(new ShippingType(data));
}