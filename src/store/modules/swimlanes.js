import {createSwimlane} from '@/models/Swimlane';
import {basestate, basegetters, baseactions, basemutations} from '@/store/basemodule'
import cloneDeep from 'lodash/cloneDeep'

const state = () => ({
    ...cloneDeep(basestate),
    entityName: 'swimlanes',
    singleEntityName: 'swimlane',
    pluralEntityName: 'swimlanes',

    schema: [
        {
            entity: 'uuid',
            label: 'UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false
        },
        {
            entity: 'flow_uuid',
            label: 'FLOW UUID',
            type: 'String',
            edit: true,
            hide: true,
            list: false
        },
        {
            entity: 'type',
            label: 'Type',
            type: 'String',
            edit: true,
            hide: true,
            list: true,
            validation: [
                v => !!v || 'Type is vereist'
            ]
        },
        {
            entity: 'name',
            label: 'Naam',
            type: 'String',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Naam is vereist'
            ]
        },
        {
            entity: 'entry_action',
            label: 'Standaard aktie',
            type: 'Select',
            edit: true,
            hide: false,
            list: true,
            options: [
                {value:'orderCreateShipment', text:'Zending aanmaken'},
                {value:'orderCreateShipmentFabrieksverkoop', text:'Zending aanmaken - Fabrieksverkoop'},
                {value:'orderSelectOutsource', text:'Uitbestedingspartij selecteren'}
            ],
            validation: []
        },
        {
            entity: 'color',
            label: 'Kleur',
            type: 'Color',
            edit: true,
            hide: false,
            list: true,
            validation: [
                v => !!v || 'Kleur is vereist'
            ]
        },
        {
            entity: 'sort_order',
            label: 'Volgorde',
            type: 'String',
            edit: true,
            hide: true,
            list: false,
            validation: [
                v => !!v || 'Volgorde is vereist'
            ]
        }
    ],
    relations: [
        {
            module: 'flows',
            type: 'many-to-one',
            fk_field: 'flow_uuid'
        },
        {
            module: 'orders',
            type: 'many-to-many',
            fk_field: ''
        }
    ]
})

export const getters = {
    ...basegetters,

    // eslint-disable-next-line no-unused-vars
    getOrders: (state, getters, rootState, rootGetters) => (uuid) => {
        const orderLinks = state.relationData.find((e) => e.uuid === uuid)?.orders

        if (!orderLinks) return [];

        const orders = orderLinks.map((orderLink) => {
            return {...rootGetters['orders/getLocalEntitiesByProps']({uuid: orderLink})[0], uuid: orderLink};
        });

        return cloneDeep(orders);
    },
}

export const actions = {
    ...baseactions,
    async fetchOrders(vuex, payload) {
        await vuex.dispatch('orders/fetchForRelation', {source: vuex.state.entityName, value: payload}, {root: true});
    },
    async storeEntity(vuex, payload) {
        return await actions.storeEntityBase(vuex, createSwimlane(payload), this.$api);
    },
}

export const mutations = {
    ...basemutations,
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}